export ZSH="$HOME/.oh-my-zsh"

# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"

plugins=(
  kubectl
  helm
  git
  history
  zsh-autosuggestions
  macos
)

source $ZSH/oh-my-zsh.sh
alias vi="nvim"
source ~/.sdkman/bin/sdkman-init.sh
eval "$(starship init zsh)"
source "$HOME/.cargo/env"
